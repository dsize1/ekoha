import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import appReducer from './appReducer'

const rootReducer = (history) => combineReducers({
  router: connectRouter(history),
  app: appReducer
})

export default rootReducer