import React, { Component, createRef} from 'react'
import { connect } from 'react-redux'
import { fetchRequested } from '../actions'

import RenderSyntaxtree from '../components/UI_component/RenderSyntaxtree'

import getExecData from '../utils/getRegExpExecData'

const baseUrl = 'http://localhost:8800/api/search?'

class Routes extends Component {
  constructor (props) {
    super(props)
    this.input = createRef()
  }

  handleFirstFetch = (e) => {
    e.preventDefault()
    const query = getExecData(/\w+(?=\/)|(?<=\/)\w+/g, this.input.current.value)
    if (query.length % 2 !== 0) {
      query.pop()
    }
    const url = query.reduce((res, token, index) => {
      if (index % 2 === 1) {
        res += '='
      }
      res += token
      return res
    }, baseUrl)
    this.props.handleFetchRequested('person_fetch_requested', url)
  }

  handleSecondFetch = (e) => {
    e.preventDefault()
    const url = this.props.app.personInfo.homeworld
    this.props.handleFetchRequested('homeworld_fetch_requested', url)
  }

  firstFetchRender = () => {
    return (
      <>
        <div>
          https://swapi.co/api/
          <input 
            type='text'
            defaultValue={'people/1'}
            ref={this.input}/>
          <button onClick={this.handleFirstFetch}>submit</button>
        </div>
      </>
    )
  }

  secondFetchRender = () => {
    if (this.props.app.fetchStatus === 'firstFetch'
      && this.props.app.personInfo === null) {
        return '404 noFound'
      }
    return (
      <>
        { 
          this.props.app.fetchStatus === 'firstFetch'
          && <button onClick={this.handleSecondFetch}>entry homework</button> 
        }
        { 
          this.props.app.fetchStatus === 'secondFetch' 
            && <RenderSyntaxtree tree={this.props.app.homeworld}/>
        }
      </>
    )
  }

  render() {
    return this.props.app.fetchStatus === '' ? this.firstFetchRender() : this.secondFetchRender()
  }
}

const mapStateToProps = (state) => ({app: state.app})
const mapDispatchToProps = (dispatch) => ({
  handleFetchRequested: (...args) => dispatch(fetchRequested(...args)) 
})

export default connect(mapStateToProps, mapDispatchToProps)(Routes)