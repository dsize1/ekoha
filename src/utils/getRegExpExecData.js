export default function (reg, str) {
  reg = RegExp(reg)
  let result = []
  let token
  while(token = reg.exec(str)) {
    result.push(token)
  }
  return result
}