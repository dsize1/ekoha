require('es6-promise').polyfill()
require('isomorphic-fetch')

const fetchData = (url, opts) => {
  return fetch(url, opts)
    .then((res) => {
      if (res.status >= 400) {
        throw new Error('Bad response from server')
      }
      return res.json()
    })
    .then((data) => {
      return data
    })

}

module.exports = {
  fetchData
}
  