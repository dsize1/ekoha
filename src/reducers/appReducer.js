const appReducer = (state, action) => {
  if(!state) {
    return {
      personInfo: null,
      homeworld: null,
      fetchStatus: ''
    }
  }
  switch (action.type) {
    case "person_fetch_done":
      return {
        personInfo: action.person,
        homeworld: null,
        fetchStatus: 'firstFetch'
      }
    case "homeworld_fetch_done":
      return {
        personInfo: state.personInfo,
        homeworld: action.homeworld,
        fetchStatus: 'secondFetch'
      }
    default:
      return state
  }
}

export default appReducer