# code测试 time:40mins
* 使用react+redux-saga 或dva实现以下内容
* 一个页面上只有一个输入框和一个按钮,按下按钮请求一个网站的api(https://swapi.co/)
* 判断这个请求的响应数据
    * 如果为空则页面显示'404'
    * 如果有内容,则页面只显示一个按钮, 按钮文案'entry_world'
    * 请求下来的的数据中含有homeworld字段,通过点击entryworld按钮,发起2次请求
        api是homeworld字段的值
    * 更新页面显示请求数据, 并且将document.title改成首次请求数据的name字段.
* 思路：搭建一个react + redux-saga 或者 dva环境
用saga去2次请求数据，坑点：浏览器不能直接发起该api的请求（需要跨域）
用express搭建一个转发请求的后台，需要用到cors这个模块，
也可以使jsonp实现跨域，但也需要自己在后端做一层jsonp的转发。