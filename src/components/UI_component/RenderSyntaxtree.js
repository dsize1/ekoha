import React from 'react'

const classOf = (val) => Object.prototype.toString.call(val).slice(8, -1)

export default function RenderSyntaxtree(tree) {
  const type = classOf(tree)
  let wrapSymbol
  if (type === 'Object') {
    wrapSymbol = '{}'
  } else if (type === 'Array') {
    wrapSymbol = '[]'
  } else {
    return tree
  }

  const subTree = (tree) => {
    return Object.entries(tree).map(([key, val]) => {
      return (
        <li key={key}>
          {`${key}: `}
            {RenderSyntaxtree(val)}
        </li>
      )
    })
  }
  return (
    <>
      <p>{wrapSymbol[0]}</p>
        <ul>{ subTree(tree) }</ul>
      <p>{wrapSymbol[1]}</p>
    </>
  )
}