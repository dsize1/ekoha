import React from 'react';
import logo from './logo.svg';
import './App.css';
import { ConnectedRouter } from "connected-react-router"
import Routes from './routes'
//foo
const App = ({history}) => (
  <ConnectedRouter history={history}>
    <Routes/>
  </ConnectedRouter>
)

export default App;
