const express = require('express')
const cors = require('cors')
const app = express()

const fetchData = require('../fetch').fetchData

// https://swapi.co/api/people/1/
const baseUrl = 'https://swapi.co/api'

app.use((req, res, next) => {
  console.log(req.url, req.method, req.body)
  next()
})

app.use(cors())

app.get('/api/search', async (req, res, next) => {
  console.log(req.query)
  const url = Object.entries(req.query).reduce((res, [key, val]) => {
    return `${res}/${key}/${val}`
  }, baseUrl)
  console.log(url)
  const data = await fetchData(url)
  console.log(data)
  res.send(JSON.stringify(data))
  next()
})

app.listen(8800, () => {
  console.log('Example app listening on port 8800')
})