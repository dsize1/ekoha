import { call, put, take, select, takeEvery, takeLatest } from 'redux-saga/effects'
const fetchData = require('../fetch').fetchData

function* fetchPerson(url) {
  try {
    const person = yield call(fetchData, url)
    // console.log(person)
    yield put({
      type: 'person_fetch_done',
      person: person
    })
  } catch(e) {
    console.log(e)
    yield put({
      type: 'person_fetch_done',
      person: null
    })
  }
}

function* fetchHomeworld(url) {
  try {
    const homeworld = yield call(fetchData, url)
    // console.log(homeworld)
    yield put({
      type: 'homeworld_fetch_done',
      homeworld: homeworld
    })
    const name = yield select((state) => state.app.personInfo.name)
    console.log(name)
    document.title = name
  } catch(e) {
    console.log(e)
    yield put({
      type: 'homeworld_fetch_done',
      homeworld: null
    })
  }
}


export default function* rootSage() {
  console.log('hello sagas!')
  const { url: firstFetchUrl } = yield take("person_fetch_requested")
  yield call(fetchPerson, firstFetchUrl)
  const { url: secondFetchUrl } = yield take("homeworld_fetch_requested")
  yield call(fetchHomeworld, secondFetchUrl)
} 